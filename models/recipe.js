const mongoose = require('mongoose');


//Model
const RecipeSchema = new mongoose.Schema({
    recipe: {
        type: String,
        unique: true
    },
    recipeDescription: String,
    img: {
        type: [String],
        default: []
    },
    price: Number,
    type: String,
    rating: {
        type: Number,
        max: 5,
        min: 1,
        default: 3
    },
    isAvailable: {
        type: Boolean,
        required: true
    },
    averageTimeToComplete: Number,
    foodType: String,
    reviews: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Review"
    }
}, {
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
        }
    }
})

const Recipe = new mongoose.model("Recipe", RecipeSchema)

module.exports = {
    Recipe
}