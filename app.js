const express = require('express');
const { json } = require('body-parser');
const session = require("express-session");
const cors = require('cors');
const { middlewares } = require('@azaxxc/common/src');
require("dotenv").config();

const { createRecipeRoute } = require('./routes/new')
const { showRecipeRouter } = require('./routes/show')
const { indexRecipeRoute } = require('./routes/index')
const { updateRecipeRouter } = require('./routes/update')

const app = express();
app.set('trust proxy', true);
app.use(json())

app.use(cors(
    {
        origin: "http://localhost:3000", // allow to server to accept request from different origin
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        credentials: true, // allow session cookie from browser to pass through
    }
))

app.use(session({
    cookie: { maxAge: 60000000 },
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}))

//routes

// app.get('/status', middlewares.authAPIs, (req, res) => {
//     res.json('HellO There');
// })

app.use(createRecipeRoute);
app.use(showRecipeRouter);
app.use(indexRecipeRoute);
app.use(updateRecipeRouter);

// app.all('*', middlewares.errorhandler)

app.use(middlewares.errorhandler);

module.exports = { app };
