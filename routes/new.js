const express = require('express')
const { middlewares } = require('@azaxxc/common/src');

const router = express.Router();
const { Recipe } = require('../models/recipe');

router.post('/api/recipes', async (req, res, err) => {
    const { recipe, recipeDescription, price, type, isAvailable } = req.body;
    let newRecipe;
    try {
        newRecipe = await Recipe.create({
            recipe,
            recipeDescription,
            price,
            type,
            isAvailable
        })

        await newRecipe.save();

        res.send(newRecipe);
    } catch (err) {
        next(err)
    }

})


module.exports = { createRecipeRoute: router }