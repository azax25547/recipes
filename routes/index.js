const express = require('express')
const { middlewares } = require('@azaxxc/common/src');
const { Recipe } = require('../models/recipe');
const { HTTPError } = require('@azaxxc/common/src/errors/Http-error');

const router = express.Router();

router.get('/api/recipes', async (req, res) => {
    try {
        const recipes = await Recipe.find({})
        if (!recipes)
            throw new HTTPError("Unable to fetch data from DB")
        res.send(recipes);
    } catch (err) {
        next(err)
    }

})


module.exports = { indexRecipeRoute: router }