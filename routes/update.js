const express = require('express')
const { middlewares, errors } = require('@azaxxc/common/src');

const router = express.Router();
const { Recipe } = require('../models/recipe');
const { HTTPError } = require('@azaxxc/common/src/errors/Http-error');

router.put('/api/recipes/:id', async (req, res, next) => {
    let { id } = req.params;
    let { ...details } = req.body;
    try {
        updatedRecipe = await Recipe.findByIdAndUpdate(id, {
            ...details
        }, {
            useFindAndModify: false,
            new: true
        })

        // if (!updatedRecipe)
        //     throw new HTTPError("Unable to find Recipe on this ID");

        res.send(updatedRecipe);
    } catch (err) {
        next(err)
    }
})


module.exports = { updateRecipeRouter: router }