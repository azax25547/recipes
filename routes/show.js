const express = require('express')
const { middlewares, errors } = require('@azaxxc/common/src');

const router = express.Router();
const { Recipe } = require('../models/recipe');

router.get('/api/recipes/:id', async (req, res, next) => {

    let recipe;
    try {
        recipe = await Recipe.findById(req.params.id);
        if (!recipe)
            throw new errors.HTTPError("Recipe's not found");

        res.send(recipe);
    } catch (err) {
        next(err)
    }

})


module.exports = { showRecipeRouter: router }